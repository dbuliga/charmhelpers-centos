# Copyright 2014-2015 Canonical Limited.
#
# This file is part of charm-helpers.
#
# charm-helpers is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# charm-helpers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with charm-helpers.  If not, see <http://www.gnu.org/licenses/>.

"""Tools for working with the host system"""
# Copyright 2012 Canonical Ltd.
#
# Authors:
#  Nick Moffitt <nick.moffitt@canonical.com>
#  Matthew Wedgwood <matthew.wedgwood@canonical.com>

from contextlib import contextmanager
from charmhelpers.core import host_factory

host = host_factory.Host()


def service_start(service_name):
    """Start a system service"""
    return host.service_start(service_name)


def service_stop(service_name):
    """Stop a system service"""
    return host.service_stop(service_name)


def service_restart(service_name):
    """Restart a system service"""
    return host.service_restart(service_name)


def service_reload(service_name, restart_on_failure=False):
    """Reload a system service, optionally falling back to restart if
    reload fails"""
    return host.service_reload(service_name, restart_on_failure)


def service_pause(service_name, init_dir="/etc/init", initd_dir="/etc/init.d"):
    """Pause a system service.

    Stop it, and prevent it from starting again at boot."""
    return host.service_pause(service_name, init_dir, initd_dir)


def service_resume(service_name, init_dir="/etc/init",
                   initd_dir="/etc/init.d"):
    """Resume a system service.

    Reenable starting again at boot. Start the service"""
    return host.service_resume(service_name, init_dir, init_dir)


def service(action, service_name):
    """Control a system service"""
    return host.service(action, service_name)


def service_running(service):
    return host.service_running(service)


def service_available(service_name):
    """Determine whether a system service is available"""
    return host.service_available(service_name)


def init_is_systemd():
    """Return True if the host system uses systemd, False otherwise."""
    return host.init_is_systemd()


def adduser(username, password=None, shell='/bin/bash', system_user=False,
            primary_group=None, secondary_groups=None):
    """
    Add a user to the system.

    Will log but otherwise succeed if the user already exists.

    :param str username: Username to create
    :param str password: Password for user; if ``None``, create a system user
    :param str shell: The default shell for the user
    :param bool system_user: Whether to create a login or system user
    :param str primary_group: Primary group for user; defaults to their username
    :param list secondary_groups: Optional list of additional groups

    :returns: The password database entry struct, as returned by `pwd.getpwnam`
    """
    return host.adduser(username, password, shell, system_user,
                        primary_group, secondary_groups)


def user_exists(username):
    """Check if a user exists"""
    return host.user_exists(username)


def add_group(group_name, system_group=False):
    return host.add_group(group_name, system_group)


def add_user_to_group(username, group):
    host.add_user_to_group(username, group)


def rsync(from_path, to_path, flags='-r', options=None):
    """Replicate the contents of a path"""
    return host.rsync(from_path, to_path, flags, options)


def symlink(source, destination):
    """Create a symbolic link"""
    host.symlink(source, destination)


def mkdir(path, owner='root', group='root', perms=0o555, force=False):
    """Create a directory"""
    host.mkdir(path, owner, group, perms, force)


def write_file(path, content, owner='root', group='root', perms=0o444):
    """Create or overwrite a file with the contents of a byte string."""
    host.write_file(path, content, owner, group, perms)


def fstab_remove(mp):
    """Remove the given mountpoint entry from /etc/fstab"""
    return host.fstab_remove(mp)


def fstab_add(dev, mp, fs, options=None):
    """Adds the given device entry to the /etc/fstab file"""
    return host.fstab_add(dev, mp, fs, options)


def mount(device, mountpoint, options=None, persist=False, filesystem="ext3"):
    """Mount a filesystem at a particular mountpoint"""
    return host.mount(device, mountpoint, options, persist, filesystem)


def umount(mountpoint, persist=False):
    """Unmount a filesystem"""
    return host.umount(mountpoint, persist)


def mounts():
    """Get a list of all mounted volumes as [[mountpoint,device],[...]]"""
    return host.mounts()


def fstab_mount(mountpoint):
    """Mount filesystem using fstab"""
    return host.fstab_mount(mountpoint)


def file_hash(path, hash_type='md5'):
    """
    Generate a hash checksum of the contents of 'path' or None if not found.

    :param str hash_type: Any hash alrgorithm supported by :mod:`hashlib`,
                          such as md5, sha1, sha256, sha512, etc.
    """
    return host.file_hash(path, hash_type)


def path_hash(path):
    """
    Generate a hash checksum of all files matching 'path'. Standard wildcards
    like '*' and '?' are supported, see documentation for the 'glob' module for
    more information.

    :return: dict: A { filename: hash } dictionary for all matched files.
                   Empty if none found.
    """
    return host.path_hash(path)


def check_hash(path, checksum, hash_type='md5'):
    """
    Validate a file using a cryptographic checksum.

    :param str checksum: Value of the checksum used to validate the file.
    :param str hash_type: Hash algorithm used to generate `checksum`.
        Can be any hash alrgorithm supported by :mod:`hashlib`,
        such as md5, sha1, sha256, sha512, etc.
    :raises ChecksumError: If the file fails the checksum

    """
    host.check_hash(path, checksum, hash_type)


class ChecksumError(ValueError):
    pass


def restart_on_change(restart_map, stopstart=False):
    """Restart services based on configuration files changing

    This function is used a decorator, for example::

        @restart_on_change({
            '/etc/ceph/ceph.conf': [ 'cinder-api', 'cinder-volume' ]
            '/etc/apache/sites-enabled/*': [ 'apache2' ]
            })
        def config_changed():
            pass  # your code here

    In this example, the cinder-api and cinder-volume services
    would be restarted if /etc/ceph/ceph.conf is changed by the
    ceph_client_changed function. The apache2 service would be
    restarted if any file matching the pattern got changed, created
    or removed. Standard wildcards are supported, see documentation
    for the 'glob' module for more information.

    @param restart_map: {path_file_name: [service_name, ...]
    @param stopstart: DEFAULT false; whether to stop, start OR restart
    @returns result from decorated function
    """
    return host.restart_on_change(restart_map, stopstart)


def restart_on_change_helper(lambda_f, restart_map, stopstart=False):
    """Helper function to perform the restart_on_change function.

    This is provided for decorators to restart services if files described
    in the restart_map have changed after an invocation of lambda_f().

    @param lambda_f: function to call.
    @param restart_map: {file: [service, ...]}
    @param stopstart: whether to stop, start or restart a service
    @returns result of lambda_f()
    """
    return host.restart_on_change_helper(lambda_f, restart_map, stopstart)


def lsb_release():
    """Return /etc/os-release in a dict"""
    return host.lsb_release()


def pwgen(length=None):
    """Generate a random pasword."""
    return host.pwgen(length)


def is_phy_iface(interface):
    """Returns True if interface is not virtual, otherwise False."""
    return host.is_phy_iface(interface)


def get_bond_master(interface):
    """Returns bond master if interface is bond slave otherwise None.

    NOTE: the provided interface is expected to be physical
    """
    return host.get_bond_master(interface)


def list_nics(nic_type=None):
    '''Return a list of nics of given type(s)'''
    return host.list_nics(nic_type)


def set_nic_mtu(nic, mtu):
    '''Set MTU on a network interface'''
    host.set_nic_mtu(nic, mtu)


def get_nic_mtu(nic):
    '''Get MTU of a network interface'''
    return host.get_nic_mtu(nic)


def get_nic_hwaddr(nic):
    return host.get_nic_hwaddr(nic)


def cmp_pkgrevno(package, revno, pkgcache=None):
    return host.cmp_pkgrevno(package, revno, pkgcache)


@contextmanager
def chdir(d):
    host.chdir(d)


def chownr(path, owner, group, follow_links=True, chowntopdir=False):
    """
    Recursively change user and group ownership of files and directories
    in given path. Doesn't chown path itself by default, only its children.

    :param bool follow_links: Also Chown links if True
    :param bool chowntopdir: Also chown path itself if True
    """
    host.chownr(path, owner, group, follow_links, chowntopdir)


def lchownr(path, owner, group):
    chownr(path, owner, group, follow_links=False)


def get_total_ram():
    '''The total amount of system RAM in bytes.

    This is what is reported by the OS, and may be overcommitted when
    there are multiple containers hosted on the same machine.
    '''
    return host.get_total_ram()
