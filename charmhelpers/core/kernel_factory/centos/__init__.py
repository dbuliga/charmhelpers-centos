import os
import subprocess

from .. import KernelBase


class Kernel(KernelBase):
    '''
    Implementation of KernelBase for CentOS
    '''

    def _modprobe(self, module, persist=True):
        if persist:
            if not os.path.exists('/etc/rc.modules'):
                open('/etc/rc.modules', 'a')
                os.chmod('/etc/rc.modules', 111)
            with open('/etc/rc.modules', 'r+') as modules:
                if module not in modules.read():
                    modules.write('modprobe %s\n' % module)

    def _update_initramfs(self, version='all'):
        """Updates an initramfs image"""
        return subprocess.check_call(["dracut", "-f", version])
