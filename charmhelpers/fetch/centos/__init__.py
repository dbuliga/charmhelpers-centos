import subprocess
import os
import time
import six
import yum

from tempfile import NamedTemporaryFile
from charmhelpers.core.hookenv import log

YUM_NO_LOCK = 1  # The return code for "couldn't acquire lock" in YUM.
YUM_NO_LOCK_RETRY_DELAY = 10  # Wait 10 seconds between apt lock checks.
YUM_NO_LOCK_RETRY_COUNT = 30  # Retry to acquire the lock X times.


def filter_installed_packages(packages):
    """Returns a list of packages that require installation"""
    yb = yum.YumBase()
    temp_cache = []
    package_list = yb.doPackageLists()
    for package in package_list['installed']:
        temp_cache.append(package.base_package_name)

    # If each package has a candidate then return the list of packages
    # that require installation
    for package in packages:
        for each in package_list['available']:
            if package not in each.base_package_name:
                log('Package {} has no installation '
                    'candidate.'.format(package), level='WARNING')
                break

    _pkgs = []
    for package in packages:
        if package not in temp_cache:
            _pkgs.append(package)
    return _pkgs


def install(packages, options=None, fatal=False):
    """Install one or more packages"""
    cmd = ['yum', '--assumeyes']
    if options is not None:
        cmd.extend(options)
    cmd.append('install')
    if isinstance(packages, six.string_types):
        cmd.append(packages)
    else:
        cmd.extend(packages)
    log("Installing {} with options: {}".format(packages,
                                                options))
    _run_yum_command(cmd, fatal)


def update(fatal=False):
    """Update local yum cache"""
    cmd = ['yum', '--assumeyes', 'update']
    log("Update with fatal: {}".format(fatal))
    _run_yum_command(cmd, fatal)


def upgrade(options=None, fatal=False, dist=False):
    """Upgrade all packages"""
    cmd = ['yum', '--assumeyes']
    if options is not None:
        cmd.extend(options)
    cmd.append('upgrade')
    log("Upgrading with options: {}".format(options))
    _run_yum_command(cmd, fatal)


def purge(packages, fatal=False):
    """Purge one or more packages"""
    cmd = ['yum', '--assumeyes', 'remove']
    if isinstance(packages, six.string_types):
        cmd.append(packages)
    else:
        cmd.extend(packages)
    log("Purging {}".format(packages))
    _run_yum_command(cmd, fatal)


def yum_search(packages):
    """Search for a package"""
    output = {}
    cmd = ['yum', 'search']
    if isinstance(packages, six.string_types):
        cmd.append(packages)
    else:
        cmd.extend(packages)
    log("Searching for {}".format(packages))
    result = subprocess.check_output(cmd)
    for package in list(packages):
        if package not in result:
            output[package] = False
        else:
            output[package] = True
    return output


def add_source(source, key=None):
    if source is None:
        log('Source is not present. Skipping')
        return

    if source.startswith('http'):
        log("Add source: {!r}".format(source))

        found = False
        # search if already exists
        directory = '/etc/yum.repos.d/'
        for filename in os.listdir(directory):
            with open(directory+filename, 'r') as rpm_file:
                if source in rpm_file:
                    found = True

        if not found:
            # write in the charms.repo
            with open(directory+'Charms.repo', 'a') as rpm_file:
                rpm_file.write('[%s]\n' % source[7:].replace('/', '_'))
                rpm_file.write('name=%s\n' % source[7:])
                rpm_file.write('baseurl=%s\n\n' % source)
    else:
        log("Unknown source: {!r}".format(source))

    if key:
        if '-----BEGIN PGP PUBLIC KEY BLOCK-----' in key:
            with NamedTemporaryFile('w+') as key_file:
                key_file.write(key)
                key_file.flush()
                key_file.seek(0)
            subprocess.check_call(['rpm', '--import', key_file])
        else:
            subprocess.check_call(['rpm', '--import', key])


def _run_yum_command(cmd, fatal=False):
    """
    Run an YUM command, checking output and retrying if the fatal flag is set
    to True.

    :param: cmd: str: The yum command to run.
    :param: fatal: bool: Whether the command's output should be checked and
        retried.
    """
    env = os.environ.copy()

    if fatal:
        retry_count = 0
        result = None

        # If the command is considered "fatal", we need to retry if the yum
        # lock was not acquired.

        while result is None or result == YUM_NO_LOCK:
            try:
                result = subprocess.check_call(cmd, env=env)
            except subprocess.CalledProcessError as e:
                retry_count = retry_count + 1
                if retry_count > YUM_NO_LOCK_RETRY_COUNT:
                    raise
                result = e.returncode
                log("Couldn't acquire YUM lock. Will retry in {} seconds."
                    "".format(YUM_NO_LOCK_RETRY_DELAY))
                time.sleep(YUM_NO_LOCK_RETRY_DELAY)

    else:
        subprocess.call(cmd, env=env)
